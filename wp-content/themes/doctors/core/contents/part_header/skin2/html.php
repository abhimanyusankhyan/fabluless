<div class="tally_header tally_header_2">
	<div class="th_branding site_content_width">
    	
        <nav class="main_menu" role="navigation"><a href="#" title="Close The Menu" class="main_menu_hand_close"><i class="fa fa-close "></i></a><?php wp_nav_menu( array('theme_location'=>'main_menu') ); ?></nav>
        <a href="#" class="main_menu_hand"><img src="<?php echo get_template_directory_uri().'/images/menuhand.png'; ?>" alt="Navigation" /></a>
    
    	<div class="th_logo">
        	<?php if(tally_option('tally_header_logo') != ''): ?>
        		<a href="<?php echo home_url(); ?>"><img src="<?php echo tally_option('tally_header_logo'); ?>" alt="<?php bloginfo('name'); ?>" /></a>
            <?php else: ?>
            	<h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
				<span><?php bloginfo( 'description' ); ?></span>
            <?php endif; ?>
        </div>
        
        <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ): ?>
        	<?php //if(class_exists('WC')): ?>
                <div class="th_woocart">
                    <a href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'doctors' ); ?>">
                        <img src="<?php echo get_template_directory_uri().'/images/woocart.png'; ?>" alt="Shopping Cart" />
                    </a>
                    <?php if(WC()->cart->cart_contents_count != '0'): ?>
                    <p>
		<?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->cart_contents_count, 'doctors' ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?>
        </p>
                    <?php endif; ?>
                </div>
            <?php //endif; ?>
        <?php endif; ?>
        
        <div class="clear"></div>
    </div>
    
</div>