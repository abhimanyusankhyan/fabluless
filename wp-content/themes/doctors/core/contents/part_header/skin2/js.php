jQuery(document).ready(function($) {
    $( "<i class='fa fa-angle-down'></i>" ).appendTo( $( ".main_menu ul.menu li.menu-item-has-children" ) );
    
    var tally_resMpanels = $('.main_menu ul.menu li.menu-item-has-children ul.sub-menu').hide();
    $('.main_menu ul.menu li.menu-item-has-children i').click(function() {
		if($(this).prev('ul.sub-menu, ul.children').hasClass('active')){
			$(this).prev('ul.sub-menu, ul.children').slideUp();
			$(this).prev('ul.sub-menu, ul.children').removeClass('active');
		}else{
			if(!$(this).parent().parent().hasClass('active')){
				tally_resMpanels.slideUp();
				tally_resMpanels.removeClass('active');
			}
			
			$(this).prev('ul.sub-menu, ul.children').slideDown();
			$(this).prev('ul.sub-menu, ul.children').addClass('active');
		}
		return false;
	});
    
	$('a.main_menu_hand').click(function() {
		if($(".main_menu ul.menu").hasClass('active')){
        
			$(".main_menu").hide();
			$(".main_menu").removeClass('active');
		}else{
			$(".main_menu").show();
			$(".main_menu").addClass('active');
		}
		return false;
	})
    
    $('a.main_menu_hand_close').click(function() {
		$(".main_menu").hide();
		$(".main_menu").removeClass('active');
		return false;
	})
});
