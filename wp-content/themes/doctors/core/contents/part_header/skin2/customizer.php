<?php
/*	Header
************************************************************* */
$wp_customize->add_section( 'tally_site_header_custom', 
	array(
		'title' => __( 'Header', 'doctors' ),
		'description' => '',
		'panel' => 'tally_headerFooter',
	) 
);

$prefix = 'tally_header_';
$section = 'tally_site_header_custom';

$uid = $prefix.'enable_woocart';
$wp_customize->add_setting( $uid, array('default' => tally_option_default($uid), 'type' => 'theme_mod', 'sanitize_callback' => 'sanitize_text_field',) );
$wp_customize->add_control( new tally_Customize_Control_Select($wp_customize, $uid, 
	array(
		'label' => __( 'Enable Shopping Cart', 'doctors' ),
		'section' => $section,
		'settings' => $uid,
		'priority' => 10,
		'description' => __('Select Yes, if you want to display the shopping cart in the header.', 'doctors'),
		'type' => 'select',
		'css_class' => 'biz',
		'choices'    => array(
			'yes'	=> __('Yes', 'doctors'),
			'no'	=> __('No', 'doctors'),
		),
	)) 
);


/*	Header Color
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
$wp_customize->add_section( 'tally_header_color', 
	array(
		'title' => __( 'Header Color', 'doctors' ),
		'description' => __('Customize the header color of the theme.', 'doctors'),
		'panel' => 'tally_color',
	) 
);

$prefix = 'tally_color_header_';
$section = 'tally_header_color';

include(TALLY_CUSTOMIZE_DRI.'/help/_text_color.php');
include(TALLY_CUSTOMIZE_DRI.'/help/_text_color_meta.php');
include(TALLY_CUSTOMIZE_DRI.'/help/_border_color.php');
include(TALLY_CUSTOMIZE_DRI.'/help/_background_color.php');
include(TALLY_CUSTOMIZE_DRI.'/help/_background_image.php');