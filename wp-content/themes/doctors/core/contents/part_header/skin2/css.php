


/* Header (2)
================================================== */
.tally_header_2{ background-color:#FFF; position: relative;  }
.tally_header_2 .th_branding{ padding-bottom:38px; padding-top: 30px; }
.tally_header_2 .th_logo{ display: block; text-align: center; }
.tally_header_2 .th_logo h1{ font-size: 20px;  margin-bottom: 0;  line-height: 30px;  text-transform: uppercase;  font-weight: bold; margin-top: 13px; }
.tally_header_2 .th_logo h1 a{ color: inherit; }
.tally_header_2 .th_logo h1 a:hover{ text-decoration: none; }
.tally_header_2 .th_logo span{ color:#9E9E9E; }
.tally_header_2 .th_woocart{ position: absolute; top: 56px; right:50px; }
.tally_header_2 .th_woocart a:hover{ opacity: 0.5; }
.tally_header_2 .th_woocart p{
	position: absolute;
	top: 53px;
	right: 0;
	width: 120px;
	border: 1px solid;
	text-align: center;
	border-radius: 40px;
}

/*~~~~~~~~~~~Menu~~~~~~~~~~~*/
.main_menu{ position: absolute; width: 100%; top:0px; left: 0; display:none; z-index: 999;   background-color: rgba(255,255,255,0.95);
 }
a.main_menu_hand{ 
	color: inherit;
	font-size: 40px;
	height: 40px;
	width: 40px;
	line-height: 40px;
	border-radius: 5px;
	position: absolute;
	top: 50px;
	left: 50px;
	text-align: center;
}
a.main_menu_hand:hover{ opacity: 0.5; }
a.main_menu_hand_close{ 
	position: absolute;
	top: 20px;
	right: 40px;
	font-size: 60px;
	color: #000;
}
a.main_menu_hand_close:hover{
	text-decoration: none;
    color: #333;
}
.main_menu ul.menu{ margin:0; padding:0; list-style-type:none; width:50%; margin-left:25%; text-align: center; margin-top:30px; }
.main_menu ul.menu li{ margin:0; padding:0; list-style-type:none; position:relative; }
.main_menu ul.menu li a{ color:#000000; font-size: 22px; line-height: 60px; height: 60px; padding-left:10px; border-bottom:solid 1px #E9EDF1; display:block; }
.main_menu ul.menu li a:hover{ text-decoration:none; }
.main_menu ul.menu li i{
	height: 60px;
	width: 60px;
	display: inline-block;
	position: absolute;
	right: 0px;
	top: 0px;
	text-align: center;
	line-height: 60px;
	cursor: pointer;
	font-size: 35px;
    border-left:solid 1px #E9EDF1;
}
.main_menu ul.menu li ul{ margin:0; }

.main_menu ul.menu li ul li a{ font-size:18px; padding-left:20px; border-bottom:solid 1px #DADADA; background-color: #F0F0F0; }
.main_menu ul.menu li ul li ul li a{ font-size:16px; padding-left:30px; border-bottom:solid 1px #BABABA; background-color: #E4E4E4 ; }




@media only screen and (max-width: 1190px){
	.tally_header_1 .th_logo{ float: none; text-align: center; }
    .tally_header_1 .th_elements{ text-align: center; margin-top:5px; clear:both; }
    .tally_header_1 .th_iconbox { text-align: left; }
}
@media only screen and (max-width: 910px) {
	.tally_header_1 .th_logo{ float: none; text-align: left; }
	a.responsive-menu-hand{ display: block; }
    .responsive-menu{ top:65px; }
    .tally_header_1 .main_menu{ display: none; }
    .tally_header_1 .th_elements{ text-align: center; margin-top:5px; clear:both; }
    .tally_header_1 .th_element{ padding:0; width:100%; border-top:solid 1px #E9EDF1; margin:0; padding-top: 10px; padding-bottom: 10px; }
    .tally_header_1 .th_element.th_social{ padding-top: 15px; padding-bottom: 15px; }
    .tally_header_1 .th_iconbox { text-align: left; display: inline-block; min-width: 220px; max-width: 100%; margin:0; }
}