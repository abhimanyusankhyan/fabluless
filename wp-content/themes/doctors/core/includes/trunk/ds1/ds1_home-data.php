<?php
$tally_home_data = array();


/*
	Home: Slideshow
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '12',
	'blocks'	=> array(
		array(
			'id'		=> 'slideshow',
			'label'		=> 'Slideshow',
			'class'		=> '',
			'name'		=> 'slideshow',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_slideshow',
	'label'			=> 'Home: Slideshow',
	'settings'		=> false, 
	'col_order'		=> array('slideshow'),
	'heading'		=> false,
	'colors'		=> false,
	'enable_column'	=> false,
	'inner_div'		=> false,
	'background'	=> false,
	'class'			=> 'tally_home_slideshow',
	'total_column'	=> '1',
	'columns'		=> $columns,
);



/*
	Home: Text Columns
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '3',
	'blocks'	=> array(
		array(
			'id'		=> 'infobox_1',
			'label'		=> '#1: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'infobox_5',
			'label'		=> '#5: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$columns[] = array(
	'class'		=> '',
	'column'	=> '3',
	'blocks'	=> array(
		array(
			'id'		=> 'infobox_2',
			'label'		=> '#2: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'infobox_6',
			'label'		=> '#6: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$columns[] = array(
	'class'		=> '',
	'column'	=> '3',
	'blocks'	=> array(
		array(
			'id'		=> 'infobox_3',
			'label'		=> '#3: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'infobox_7',
			'label'		=> '#7: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$columns[] = array(
	'class'		=> '',
	'column'	=> '3',
	'blocks'	=> array(
		array(
			'id'		=> 'infobox_4',
			'label'		=> '#4: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'infobox_8',
			'label'		=> '#8: Infobox',
			'class'		=> '',
			'name'		=> 'info_box',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_infoBox',
	'label'			=> 'Home: Infobox',
	'settings'		=> true, 
	'col_order'		=> array('infobox_1', 'infobox_2', 'infobox_3', 'infobox_4', 'infobox_5', 'infobox_6', 'infobox_7', 'infobox_8'),
	'heading'		=> true,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> false,
	'background'	=> true,
	'class'			=> 'tally_home_infoBox',
	'total_column'	=> '4',
	'columns'		=> $columns,
);


/*
	Home: InfoBox
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '6',
	'blocks'	=> array(
		array(
			'id'		=> 'text_1',
			'label'		=> 'Left Text',
			'class'		=> '',
			'name'		=> 'text',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$columns[] = array(
	'class'		=> '',
	'column'	=> '6',
	'blocks'	=> array(
		array(
			'id'		=> 'text_2',
			'label'		=> 'Right Text',
			'class'		=> '',
			'name'		=> 'text',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_text',
	'label'			=> 'Home: Text Columns',
	'settings'		=> true, 
	'col_order'		=> array('text_1', 'text_2'),
	'heading'		=> false,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> true,
	'background'	=> true,
	'class'			=> 'tally_home_text',
	'total_column'	=> '2',
	'columns'		=> $columns,
);



/*
	Home: Testimonials
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '6',
	'blocks'	=> array(
		array(
			'id'		=> 'testimonial_1',
			'label'		=> '#1: Testimonial',
			'class'		=> '',
			'name'		=> 'testimonial',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'testimonial_3',
			'label'		=> '#3: Testimonial',
			'class'		=> '',
			'name'		=> 'testimonial',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$columns[] = array(
	'class'		=> '',
	'column'	=> '6',
	'blocks'	=> array(
		array(
			'id'		=> 'testimonial_2',
			'label'		=> '#2: Testimonial',
			'class'		=> '',
			'name'		=> 'testimonial',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
		array(
			'id'		=> 'testimonial_4',
			'label'		=> '#4: Testimonial',
			'class'		=> '',
			'name'		=> 'testimonial',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_testimonial',
	'label'			=> 'Home: Testimonials',
	'settings'		=> true, 
	'col_order'		=> array('testimonial_1', 'testimonial_2', 'testimonial_3', 'testimonial_4'),
	'heading'		=> true,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> true,
	'background'	=> true,
	'class'			=> 'tally_home_testimonial',
	'total_column'	=> '2',
	'columns'		=> $columns,
);




/*
	Home: Parallax
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '12',
	'blocks'	=> array(
		array(
			'id'		=> 'parallax',
			'label'		=> 'Parallax Text',
			'class'		=> '',
			'name'		=> 'text',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_parallax1',
	'label'			=> 'Home: Parallax #1',
	'settings'		=> true, 
	'col_order'		=> array('parallax'),
	'heading'		=> false,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> true,
	'background'	=> true,
	'class'			=> 'tally_home_parallax',
	'total_column'	=> '1',
	'columns'		=> $columns,
);




/*
	Home: Blog
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '12',
	'blocks'	=> array(
		array(
			'id'		=> 'blog',
			'label'		=> 'Blog Grid',
			'class'		=> '',
			'name'		=> 'blog_grid',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_blog',
	'label'			=> 'Home: Blog',
	'settings'		=> true, 
	'col_order'		=> array('blog'),
	'heading'		=> true,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> true,
	'background'	=> true,
	'class'			=> 'tally_home_blog',
	'total_column'	=> '1',
	'columns'		=> $columns,
);



/*
	Home: Parallax 2
------------------------------------------*/
$columns = array();
$columns[] = array(
	'class'		=> '',
	'column'	=> '12',
	'blocks'	=> array(
		array(
			'id'		=> 'parallax',
			'label'		=> 'Parallax Text',
			'class'		=> '',
			'name'		=> 'text',
			'skin'		=> 'skin1',
			'biz'		=> false,
		),
	)
);
$tally_home_data[] = array(
	'id'			=> 'tally_home_parallax2',
	'label'			=> 'Home: Parallax #2',
	'settings'		=> true, 
	'col_order'		=> array('parallax'),
	'heading'		=> false,
	'colors'		=> true,
	'enable_column'	=> true,
	'inner_div'		=> true,
	'background'	=> true,
	'class'			=> 'tally_home_parallax',
	'total_column'	=> '1',
	'columns'		=> $columns,
);