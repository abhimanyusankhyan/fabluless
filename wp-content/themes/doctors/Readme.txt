== Tally Framework ==
Medical WordPress Theme.


== Theme License & Copyright ==
Doctors WordPress theme, Copyright (C) 2015 TallyThemes
Doctors WordPress theme is licensed under the GPL.


== License == 
This theme, like WordPress, is licensed under the GPL 2.0.

Flexslider - http://www.woothemes.com/flexslider/
License: http://opensource.org/licenses/MIT
Copyright: WooThemes - http://www.woothemes.com


Images -
I have included some images for demo perpost. All the images from https://pixabay.com/ and License under CC0 Public Domain
License Link: https://pixabay.com/en/service/terms/#download_terms
And It allow us to include images in the theme.


== How to Use the Theme ==
We have created a well documentation of the theme please go http://tallythemes.com/doc-item/doctors-wordpress-theme-documentation/