<?php
include(get_template_directory().'/core/core.php');
function moesia_scripts() {
wp_enqueue_style( 'Slider-Style', get_template_directory_uri() . '/app/assets/css/slider.css', array(), true );
wp_enqueue_script( 'slider-side', get_template_directory_uri() .  '/js/slider.js', array(), true );

}
add_action( 'wp_enqueue_scripts', 'moesia_scripts' );