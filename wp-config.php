<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fabuless');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'djPk,yvFp$?&4>4yO#~IxDhda:`?<gMwirhk1_EC^NxTM[O1[[8y?[m0b5^o0QC9');
define('SECURE_AUTH_KEY',  '9^D;aHCVyZkhUGDCtk~rGSqz)1i%``#ABCC,81;d5F3>uid k_6B W]W&bVQ;E&G');
define('LOGGED_IN_KEY',    'dWf}aNB MOCRID5Ql]rV<[]qj44.?rjtw/H_K(nW3@ov>d<ErxR{so]$nLm0A$]^');
define('NONCE_KEY',        'acQ=&ikw`r7dN ,t$eEElxkQw)/9gOm{M6E Yv]JLg*a$-)1%:o)v.S[NhOyk3wy');
define('AUTH_SALT',        '3_)hkI-G:b9iyi]9Ahjp3U0Z@[w_C9rl?_Ie**-`nvtbb1i<2}T~lwS]SV+`j%r=');
define('SECURE_AUTH_SALT', 'CVxq3D7DVT EoZ4THb?#B9WlH>aZiOz7;65&freRqgkH#k*`VimFyLx6TId!Q~?.');
define('LOGGED_IN_SALT',   ';FU[YX?Ma2<q}z?{kYctC4g!ZVS9Aw%:0;@$b|N0KIwq@]x/LZ2^1|uB-?J{_*ps');
define('NONCE_SALT',       'vHL;/`}*73>11Vn~G3Bd8d+eK^U}kT4l(RWvHEIaL+].xY57^L<BA:<pOnn){{zs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
